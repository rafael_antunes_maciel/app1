pipeline {

    environment {
        PROJECT                 = "natura-images"
        IMAGE_NAME              = ""
        DOCKER_DOMAIN           = "973281175846.dkr.ecr.us-east-1.amazonaws.com"
        REGION                  = "us-east-1"
        EKS_CLUSTER_NAME        = "eks_cluster_natura"
        BUCKET_PROD             = "natura-prd-teste"
        BUCKET_HLG              = "natura-hml-teste"
        BUCKET_DEV              = "natura-dev-teste"

                
    }

agent any

    stages {

        stage('Setup') {
            steps {
                script {
                    dockerTag = sh (script: "git log -n 1 --pretty=format:'%h'", returnStdout: true)

                    IMAGE_NAME = "${DOCKER_DOMAIN}/${PROJECT}:nginx-${dockerTag}"

                    IMAGE_NAME_LATEST = "${DOCKER_DOMAIN}/${PROJECT}:latest"
                    IMAGE_NAME_HML = "${DOCKER_DOMAIN}/${PROJECT}:hml"
                    IMAGE_NAME_DEV = "${DOCKER_DOMAIN}/${PROJECT}:dev"


    
                }
            }
        }

        stage('Build Development') {
            when { branch 'DEV' }
            steps {
                echo "Building application and Docker image"
                script {
      
                    sh "sed -i 's#{ENV}#dev#g' nginx.conf"
                           
                    sh "docker build -t $IMAGE_NAME ."
       
                    sh "docker tag $IMAGE_NAME $IMAGE_NAME_DEV"
                }
            }
        }

        stage('Build Homologation') {
            when { branch 'HML' }
            steps {
                echo "Building application and Docker image"
                script {
      
                    sh "sed -i 's#{ENV}#hml#g' nginx.conf"
                           
                    sh "docker build -t $IMAGE_NAME ."
       
                    sh "docker tag $IMAGE_NAME $IMAGE_NAME_HML"
                }
            }
        }

        stage('Build Production') {
            when { branch 'master' }
            steps {
                echo "Building application and Docker image"
                script {
      
                    sh "sed -i 's#{ENV}#prd#g' nginx.conf"
                           
                    sh "docker build -t $IMAGE_NAME ."
       
                    sh "docker tag $IMAGE_NAME $IMAGE_NAME_LATEST"
                }
            }
        }            

        stage('Test Config NGinx') {
            steps {
                echo "Testing Config"
                script {
                             
                    sh "docker run --cpus=1 --rm --interactive $IMAGE_NAME nginx -t"
                }
            }
        }

        stage('Upload to S3 Development') {
            when { branch 'DEV' }
            steps {
                echo 'Upload to S3 Development'
                script {
                    
                    sh "sed -i 's#ENVIRONMENT#DEV#g' index.html"
                        
                    withAWS(region: 'us-east-1') {
                    s3Upload(file: 'index.html', bucket: "${BUCKET_DEV}", path: '', acl: 'PublicRead')
                    }
                }      
            }
        }

        stage('Upload to S3 Homologation') {
            when { branch 'HML' }
            steps {
                echo 'Upload to S3 Homologation'
                script {
                    
                    sh "sed -i 's#ENVIRONMENT#HML#g' index.html"
                        
                    withAWS(region: 'us-east-1') {
                    s3Upload(file: 'index.html', bucket: "${BUCKET_HLG}", path: '', acl: 'PublicRead')
                    }
                }      
            }
        }

        stage('Upload to S3 Production') {
            when { branch 'master' }
            steps {
                echo 'Upload to S3 Production'
                script {
                    
                    sh "sed -i 's#ENVIRONMENT#PRD#g' index.html"
                        
                    withAWS(region: 'us-east-1') {
                    s3Upload(file: 'index.html', bucket: "${BUCKET_PROD}", path: '', acl: 'PublicRead')
                    }
                }      
            }
        }

        stage('Push Development') {
            when { branch 'DEV' }
            steps {
                echo "Pushing image"
                script {
                    
                    sh "aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $DOCKER_DOMAIN"
                          
                    sh "docker push $IMAGE_NAME"

                    sh "docker push $IMAGE_NAME_DEV"

                    sh "docker rmi $IMAGE_NAME"

                    sh "docker rmi $IMAGE_NAME_DEV"
                    
                }
            }
        }

        stage('Push Homologation') {
            when { branch 'HML' }
            steps {
                echo "Pushing image"
                script {
                    
                    sh "aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $DOCKER_DOMAIN"
                          
                    sh "docker push $IMAGE_NAME"

                    sh "docker push $IMAGE_NAME_HML"

                    sh "docker rmi $IMAGE_NAME"

                    sh "docker rmi $IMAGE_NAME_HML"
                    

                }
            }
        }

        stage('Push Production') {
            when { branch 'master' }
            steps {
                echo "Pushing image"
                script {
                    
                    sh "aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $DOCKER_DOMAIN"
                          
                    sh "docker push $IMAGE_NAME"

                    sh "docker push $IMAGE_NAME_LATEST"

                    sh "docker rmi $IMAGE_NAME"

                    sh "docker rmi $IMAGE_NAME_LATEST"
                    
                }
            }
        }

        stage('Deploy Development') {
            when { branch 'DEV' }
            steps {
                echo "Delete image and update stack"
                script {
                    
                    sh "aws eks --region $REGION update-kubeconfig --name $EKS_CLUSTER_NAME"
                          
                    sh "sed -i 's#{IMAGE_DEPLOY}#${IMAGE_NAME}#g' app.yaml"

                    sh "sed -i 's#{NAMESPACE_DEPLOY}#dev#g' app.yaml"                          

                    sh "kubectl apply -f app.yaml"

                }
            }
        }

        stage('Deploy Homologation') {
            when { branch 'HML' }
            steps {
                echo "Delete image and update stack"
                script {
                    
                    sh "aws eks --region $REGION update-kubeconfig --name $EKS_CLUSTER_NAME"
                          
                    sh "sed -i 's#{IMAGE_DEPLOY}#${IMAGE_NAME}#g' app.yaml"

                    sh "sed -i 's#{NAMESPACE_DEPLOY}#hml#g' app.yaml"                          

                    sh "kubectl apply -f app.yaml"
                          
                }
            }
        }

        stage('Deploy Production') {
            when { branch 'master' }
            steps {
                echo "Delete image and update stack"
                script {
                    
                    sh "aws eks --region $REGION update-kubeconfig --name $EKS_CLUSTER_NAME"
                          
                    sh "sed -i 's#{IMAGE_DEPLOY}#${IMAGE_NAME}#g' app.yaml"

                    sh "sed -i 's#{NAMESPACE_DEPLOY}#prd#g' app.yaml"                          

                    sh "kubectl apply -f app.yaml"
                          
                }
            }
        }


    }

    post {

        always {
            sh "docker rmi $IMAGE_NAME -f || true"
            sh "docker rmi $IMAGE_NAME_LATEST -f || true"
            sh "docker rmi $IMAGE_NAME_HML -f || true"
            sh "docker rmi $IMAGE_NAME_DEV -f || true"                                    
            cleanWs()
            deleteDir()
        }
    }
}