user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log debug;
pid        /var/run/nginx.pid;

events {
    use epoll;
    worker_connections 21000;
    multi_accept on;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    proxy_cache_path   /tmp/ levels=1:2 keys_zone=s3_cache:10m max_size=500m
                     inactive=60m use_temp_path=off;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    open_file_cache off;

    client_max_body_size 50M;
    client_body_buffer_size 1m;
    client_body_timeout 15;
    client_header_timeout 15;
    send_timeout 15;

    fastcgi_buffers 256 16k;
    fastcgi_buffer_size 128k;
    fastcgi_connect_timeout 10s;
    fastcgi_send_timeout 20s;
    fastcgi_read_timeout 120s;
    fastcgi_busy_buffers_size 256k;
    fastcgi_temp_file_write_size 256k;
    reset_timedout_connection on;
    server_names_hash_bucket_size 100;

    server_tokens off;

    log_format graylog_json '{ "timestamp": "$time_iso8601", "remote_addr": "$remote_addr",'
                            ' "body_bytes_sent": $body_bytes_sent, "request_time": $request_time,'
                            ' "response_status": $status, "request": "$request", "http_origin": "$http_origin",'
                            ' "request_method": "$request_method", "host": "$host",'
                            ' "upstream_cache_status": "$upstream_cache_status", "upstream_addr": "$upstream_addr",'
                            ' "http_x_forwarded_for": "$http_x_forwarded_for", "http_referrer": "$http_referer",'
                            ' "http_user_agent": "$http_user_agent", "http_version": "$server_protocol" }';

    access_log /dev/stdout graylog_json;
    error_log /dev/stderr;

    server {
    listen 80;
    ssl off;

        location / {
            try_files $uri @s3;
        }

        location @s3 {
          set $s3_bucket        'natura-{ENV}-teste.s3-website-us-east-1.amazonaws.com';
          set $url_full         '$1';

          proxy_http_version     1.1;
          proxy_set_header       Host $s3_bucket;
          proxy_set_header       Authorization '';
          proxy_hide_header      x-amz-id-2;
          proxy_hide_header      x-amz-request-id;
          proxy_hide_header      x-amz-meta-server-side-encryption;
          proxy_hide_header      x-amz-server-side-encryption;
          proxy_hide_header      Set-Cookie;
          proxy_ignore_headers   Set-Cookie;
          proxy_intercept_errors on;

          resolver               8.8.4.4 8.8.8.8 valid=300s;
          resolver_timeout       10s;
          proxy_pass             http://$s3_bucket$url_full;
        }
    }

}